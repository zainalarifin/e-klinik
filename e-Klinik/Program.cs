﻿using System;

namespace e_Klinik
{
    class Program
    {
        static bool keluar = false;
        static int menu;
        static String input;
        static void Main(string[] args)
        {

            do
            {
                Console.Clear();
                Console.WriteLine("==========================");
                Console.WriteLine("         E-Klinik");
                Console.WriteLine("==========================");
                Console.WriteLine("1. Login");
                Console.WriteLine("2. About");
                Console.WriteLine("3. Exit");
                Console.WriteLine("Pilih Menu ?");
                input = Console.ReadLine();
                menu = Convert.ToInt16(input);
                switch (menu)
                {
                    case 1:
                        Console.Clear();
                        Login doLogin = new Login();
                        if (doLogin.FormLogin())
                        {
                            Console.Clear();
                            MenuKlinik klinik = new MenuKlinik();
                            String username = doLogin.GetUsername();
                            String sebagai = doLogin.GetSebagai();
                            klinik.ShowMenu(username, sebagai);
                        }
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("Aplikasi E-Klinik merupakan aplikasi untuk mendaftarkan");
                        Console.WriteLine("Pasien yang ingin melakukan pemeriksaan kesehatan,");
                        Console.WriteLine("Aplikasi ini mampu mencatat informasi Pasien, Diagnosa, dan Obat.");
                        Console.WriteLine("Aplikasi ini masih demo, jika ingin menggunakan secara fungsional,");
                        Console.WriteLine("Silahkan hubungi : ");
                        Console.WriteLine("Zainal Arifin - 0902318");
                        Console.WriteLine("081910126583 ");
                        Console.WriteLine("arifin.1602@gmail.com");
                        Console.WriteLine("http://zainalarifin.id");
                        Console.WriteLine("Press Any Key to Back ...");
                        input = Console.ReadLine();
                        break;
                    case 3:
                        keluar = true;
                        break;
                }
            } while (!keluar);
        }
    }
}
