﻿using System;
using System.Collections.Generic;

namespace e_Klinik
{
    class Pasien
    {
        private Dictionary<string, Dictionary<string, string>> listPasien = new Dictionary<string, Dictionary<string, string>>();
        private string idPasien;
        private string namaPasien;
        private string alamatPasien;
        private string tempatLahir;
        private string tanggalLahir;
        private string noTelepon;
        private string statusAktif;

        public Dictionary<string, Dictionary<string, string>> ListPasien
        {
            get
            {
                return listPasien;
            }
        }

        public string IdPasien
        {
            get
            {
                return idPasien;
            }

            set
            {
                idPasien = value;
            }
        }

        public string NamaPasien
        {
            get
            {
                return namaPasien;
            }

            set
            {
                namaPasien = value;
            }
        }

        public string AlamatPasien
        {
            get
            {
                return alamatPasien;
            }

            set
            {
                alamatPasien = value;
            }
        }

        public string TempatLahir
        {
            get
            {
                return tempatLahir;
            }

            set
            {
                tempatLahir = value;
            }
        }

        public string TanggalLahir
        {
            get
            {
                return tanggalLahir;
            }

            set
            {
                tanggalLahir = value;
            }
        }

        public string NoTelepon
        {
            get
            {
                return noTelepon;
            }

            set
            {
                noTelepon = value;
            }
        }

        public string StatusAktif
        {
            get
            {
                return statusAktif;
            }

            set
            {
                statusAktif = value;
            }
        }

        public void SimpanDataPasien()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("idPasien", IdPasien);
            data.Add("namaPasien", NamaPasien);
            data.Add("alamatPasien", AlamatPasien);
            data.Add("tempatLahir", TempatLahir);
            data.Add("tanggalLahir", TanggalLahir);
            data.Add("noTelepon", NoTelepon);
            data.Add("statusAktif", StatusAktif);
            //Dictionary<string, Dictionary<string, string>> pasien = new Dictionary<string, Dictionary<string, string>>();
            //pasien.Add(IdPasien, data);
            ListPasien.Add(IdPasien, data);
        }
    }
}
