﻿using System;

namespace e_Klinik
{
    class Login
    {
        private String username;
        private String sebagai;

        public bool FormLogin()
        {
            Console.WriteLine("=======================");
            Console.WriteLine("        Login");
            Console.WriteLine("=======================");
            Console.Write("Username : ");
            username = Console.ReadLine();
            Console.Write("sebagai : ");
            sebagai = Console.ReadLine();
            return true;
        }

        public String GetUsername() {
            return username;
        }

        public String GetSebagai() {
            return sebagai;
        }
    }
}
