﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e_Klinik
{
    class Pengguna
    {
        Dictionary<string, Dictionary<string, string>> listPengguna = new Dictionary<string, Dictionary<string, string>> {
            
        };
        private string idPengguna;
        private string namaPengguna;
        private string alamatPengguna;
        private string noTelepon;
        private string username;
        private string password;
        private string tipePengguna;
        private string statusAktif;

        public Dictionary<string, Dictionary<string, string>> ListPengguna
        {
            get
            {
                return listPengguna;
            }
        }

        public string IdPengguna
        {
            get
            {
                return idPengguna;
            }

            set
            {
                idPengguna = value;
            }
        }

        public string NamaPengguna
        {
            get
            {
                return namaPengguna;
            }

            set
            {
                namaPengguna = value;
            }
        }

        public string AlamatPengguna
        {
            get
            {
                return alamatPengguna;
            }

            set
            {
                alamatPengguna = value;
            }
        }

        public string NoTelepon
        {
            get
            {
                return noTelepon;
            }

            set
            {
                noTelepon = value;
            }
        }

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string TipePengguna
        {
            get
            {
                return tipePengguna;
            }

            set
            {
                tipePengguna = value;
            }
        }

        public string StatusAktif
        {
            get
            {
                return statusAktif;
            }

            set
            {
                statusAktif = value;
            }
        }


        public void SimpanDataPengguna()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("idPengguna", IdPengguna);
            data.Add("namaPengguna", NamaPengguna);
            data.Add("alamatPengguna", AlamatPengguna);
            data.Add("noTelepon", NoTelepon);
            data.Add("username", Username);
            data.Add("password", Password);
            data.Add("tipePengguna", TipePengguna);
            data.Add("statusAktif", StatusAktif);
            ListPengguna.Add(IdPengguna, data);
        }

    }
}
