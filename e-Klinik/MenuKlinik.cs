﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace e_Klinik
{
    class MenuKlinik
    {
        private bool keluar;
        private String menu;
        private String username;
        private String sebagai;

        private void Garis()
        {
            Console.WriteLine("=============================================");
        }

        public void ShowMenu(String i_username, String i_sebagai)
        {
            username = i_username;
            sebagai = i_sebagai;
            Pasien pasien = new Pasien();
            Pengguna pengguna = new Pengguna();
            if (sebagai == "admin")
            {
                
                do
                {
                    Console.Clear();
                    Console.WriteLine("Hai, {0}", username);
                    Console.WriteLine("1. Registrasi Pasien Baru");
                    Console.WriteLine("2. Registrasi Pasien Lama");
                    Console.WriteLine("3. Logout");
                    Console.WriteLine("Pilih menu ?");
                    menu = Console.ReadLine();
                    switch (menu)
                    {
                        case "1":
                            Garis();
                            Console.WriteLine("            Registrasi Pasien Baru");
                            Garis();
                            try
                            {
                                Console.WriteLine("|Daftar Pasien|");
                                Console.WriteLine("| Id Pasien | Nama Pasien | Alamat Pasien |");
                                foreach (KeyValuePair<string, Dictionary<string, string> > data in pasien.ListPasien)
                                {
                                    Console.WriteLine("{0} | {1} | {2}", data.Key, data.Value["namaPasien"], data.Value["alamatPasien"]);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("+++++ List data pasien kosong +++++");
                            }

                            Console.Write("ID Pasien     : ");
                            pasien.IdPasien = Console.ReadLine();
                            Console.Write("Nama Lengkap  : ");
                            pasien.NamaPasien = Console.ReadLine();
                            Console.Write("Alamat        : ");
                            pasien.AlamatPasien = Console.ReadLine();
                            Console.Write("Tempat Lahir  : ");
                            pasien.TempatLahir = Console.ReadLine();
                            Console.Write("Tanggal Lahir : ");
                            pasien.TanggalLahir = Console.ReadLine();
                            Console.Write("No Telepon    : ");
                            pasien.NoTelepon = Console.ReadLine();
                            Console.Write("Aktif         : ");
                            pasien.StatusAktif = Console.ReadLine();
                            pasien.SimpanDataPasien();
                            Console.Write("Tekan apa saja untuk kembali. . .");
                            Console.ReadKey();
                            break;
                        case "2":
                            string kodePasien;
                            Garis();
                            Console.WriteLine("           Registrasi Ulang Pasien");
                            Garis();
                            Console.WriteLine("                 List Pasien");
                            Garis();
                            foreach (KeyValuePair<string, Dictionary<string, string>> data in pasien.ListPasien)
                            {
                                Console.WriteLine("{0} | {1} | {2}", data.Key, data.Value["namaPasien"], data.Value["alamatPasien"]);
                            }
                            Garis();
                            Console.WriteLine("                 List Dokter");
                            
                            foreach (KeyValuePair<string, Dictionary<string, string>> data in pengguna.ListPengguna)
                            {
                                //if(data.Value["tipePengguna"])
                                //{
                                    Console.WriteLine("{0} | {1} | {2}", data.Key, data.Value["namaPengguna"], data.Value["tipePengguna"]);
                                //}
                                
                            }
                            Console.Write("KodePasien: ");
                            kodePasien = Console.ReadLine();
                            Console.WriteLine("Pasien telah didaftarkan");
                            Console.WriteLine("Tekan apa saja untuk kembali. . .");
                            Console.ReadKey();
                            break;
                        case "3":
                            Console.WriteLine("Anda Telah Logout.");
                            Console.WriteLine("Tekan apa saja untuk kembali ke halaman awal. . .");
                            menu = Console.ReadLine();
                            keluar = true;
                            break;
                    }
                } while (!keluar);
            }
            else if(sebagai == "dokter")
            {
                do
                {
                    Console.Clear();
                    Console.WriteLine("Hai, Dr.{0}", username);
                    Console.WriteLine("1. Lihat List Pasien");
                    Console.WriteLine("2. Diagnosa Pasien");
                    Console.WriteLine("3. Logout");
                    Console.WriteLine("Pilih menu ?");
                    menu = Console.ReadLine();
                    switch (menu)
                    {
                        case "1":
                            Console.Clear();
                            Console.WriteLine("=======================");
                            Console.WriteLine("      List Pasien");
                            Console.WriteLine("=======================");
                            menu = Console.ReadLine();
                            break;
                        case "2":
                            Console.Clear();
                            Console.WriteLine("===================================");
                            Console.WriteLine("      Diagnosa Pasien");
                            Console.WriteLine("===================================");
                            menu = Console.ReadLine();
                            break;
                        case "3":
                            Console.Clear();
                            Console.WriteLine("Anda Telah Logout.");
                            Console.WriteLine("Tekan apa saja untuk kembali ke halaman awal.");
                            menu = Console.ReadLine();
                            keluar = true;
                            break;
                    }
                } while (!keluar);
            }
        }
    }
}
